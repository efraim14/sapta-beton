<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class MakeTblAAMConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblAAMConfig', function (Blueprint $table) {
            $table->increments('ConfigId');
            $table->String('ConfigName');
            $table->text('Value')->nullable();
            $table->boolean('IsActive')->default(1);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblAAMConfig');
    }
}
