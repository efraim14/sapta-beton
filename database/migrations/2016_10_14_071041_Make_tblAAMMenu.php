<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeTblAAMMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblAAMMenu', function (Blueprint $table) {
            $table->increments('MenuId');
            $table->String('MenuName');
            $table->String('MenuController');
            $table->String('Description')->nullable();
            $table->boolean('IsActive')->default(1);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblAAMMenu');
    }
}
