<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblAAFContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblAATForm', function (Blueprint $table) {
            $table->increments('FormId');
            $table->string('Name');
            $table->string('Email');
            $table->integer('Type');
            $table->text('Message')->nullable();
            $table->string('Attachment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblAATForm');
    }
}
