<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblAAMBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblAAMBanners', function (Blueprint $table) {
            $table->increments('BannerId');
            $table->string('Name');
            $table->string('Image');
            $table->string('Description')->nullable();
            $table->text('SubDescription')->nullable();
            $table->boolean('IsActive')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblAAMBanners');
    }
}
