<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(tblAAMConfigSeeder::class);
        $this->call(tblAAMMenuSeeder::class);
        $this->call(tblAAMBannerSeeder::class);
    }
}
