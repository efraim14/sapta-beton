<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class tblAAMMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          // Insert config
        DB::table('tblAAMMenu')->insert([
            [
                'MenuName' => 'Home',
                'MenuController' => 'home',
                'Description' => 'Landing Page for Main Website',
                'IsActive' => 1,
                'created_at' => Carbon::now('Asia/Jakarta')
            ],
            [
                'MenuName' => 'Products & Clients',
                'MenuController' => 'products',
                'Description' => 'Products about Company',
                'IsActive' => 1,
                'created_at' => Carbon::now('Asia/Jakarta')
            ],
            [
                'MenuName' => 'Gallery',
                'MenuController' => 'gallery',
                'Description' => 'Our Galleries',
                'IsActive' => 1,
                'created_at' => Carbon::now('Asia/Jakarta')
            ],
            [
                'MenuName' => 'Career',
                'MenuController' => 'career',
                'Description' => 'Page about opportunity career',
                'IsActive' => 1,
                'created_at' => Carbon::now('Asia/Jakarta')
            ],
            [
                'MenuName' => 'Contact',
                'MenuController' => '#contact',
                'Description' => 'Contact Us',
                'IsActive' => 1,
                'created_at' => Carbon::now('Asia/Jakarta')
            ],
            [
                'MenuName' => 'Login',
                'MenuController' => 'login',
                'Description' => 'Admin Login',
                'IsActive' => 0,
                'created_at' => Carbon::now('Asia/Jakarta')
            ],
            [
                'MenuName' => 'Register',
                'MenuController' => 'register',
                'Description' => 'Admin Register',
                'IsActive' => 0,
                'created_at' => Carbon::now('Asia/Jakarta')
            ]
        ]);
    }
}
