<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class tblAAMConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         // Insert config
        DB::table('tblAAMConfig')->insert([
            [
                'ConfigName' => 'Call',
                'Value' => '+12345678',
                'created_at' => Carbon::now('Asia/Jakarta')
            ],
            [
                'ConfigName' => 'Email',
                'Value' => 'example@example.com',
                'created_at' => Carbon::now('Asia/Jakarta')
            ],
            [
                'ConfigName' => 'AboutUs',
                'Value' => '',
                'created_at' => Carbon::now('Asia/Jakarta')
            ],
            [
                'ConfigName' => 'WebsiteName',
                'Value' => 'PT. Afindy Agung',
                'created_at' => Carbon::now('Asia/Jakarta')
            ],
            [
                'ConfigName' => 'BackgroundName',
                'Value' => 'bg.jpg',
                'created_at' => Carbon::now('Asia/Jakarta')
            ]
        ]);
    }
}
