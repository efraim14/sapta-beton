<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class tblAAMBannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert config
        DB::table('tblAAMBanners')->insert([
            [
                'Name' => 'Banner 1',
                'Image' => 'banner1.jpg',
                'Description' => 'Sapta Beton',
                'SubDescription' => 'Quality and Trust You Will Remember',
                'created_at' => Carbon::now('Asia/Jakarta')
            ],
            [
                'Name' => 'Banner 2',
                'Image' => 'banner2.jpg',
                'Description' => 'We are here',
                'SubDescription' => 'since 1989',
                'created_at' => Carbon::now('Asia/Jakarta')
            ],
            [
                'Name' => 'Banner 3',
                'Image' => 'banner3.jpg',
                'Description' => 'Your',
                'SubDescription' => 'Infrastructure solution',
                'created_at' => Carbon::now('Asia/Jakarta')
            ]
        ]);
    }
}
