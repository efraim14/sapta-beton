<?php

return array (
  'websiteName' => 'Sapta Beton',
  'contactUs' => '(021) - 6266325/26 Hunting',
  'mailUs' => 'marketing@saptabeton.com',
  'address' => 'Sapta Beton',
  'city' => 'Jakarta',
  'cb' => 
  array (
    'title' => 'Company Background',
    'header' => 'To be the best in the supply of',
    'subheader' => 'Ready-Mix',
    'text' => 'Sapta Beton established and operated since 1989, expanded it\'s business to the provision of ready-mix concrete products. Following with a rapid growth in economic and infrastructure development, Sapta Beton ready to be the preferred solution of industrial projects, high-rise buildings, housing, roads and infrastructure that requiring Ready-Mix.',
    'submenu1' => 'Standard Quality',
    'submenutext1' => 'Fulfill the needs of standard quality.',
    'submenu2' => 'Tools and Facilities',
    'submenutext2' => 'Supported by our highest facilities and reliable tools.',
    'subbox1' => 'Accurate ',
    'subboxtext1' => 'Our quality concrete processing help us meets client\'s needs',
    'subbox2' => 'Tools',
    'subboxtext2' => 'Highest quality of tools to ensure the best products',
    'subbox3' => 'Facilities',
    'subboxtext3' => 'We use the best facilities in Sapta Beton',
    'subbox4' => 'Quality',
    'subboxtext4' => 'Along with our experience, we ensure to produce the best quality ',
  ),
  'dev' => 
  array (
    'title' => 'Our Developments',
    'title1' => 'Our Marketing',
    'subtitle1' => 'Excellence',
    'subtext1' => 'Sapta Beton has a strategic location, close to the toll road so it can easily distribute concrete products for client\'s projects in Jakarta and Banten. The target of our projects are Industrial, High-rise buildings, Housing, Roads and Infrastructure that requires concrete / ready-mix ',
    'mark1-1' => 'Strategic Location',
    'mark1-2' => 'High Quality',
    'subtitle2' => 'Marketing',
    'subtext2' => 'Our marketing strategy is to reach a stable market with retail marketing strategy. The benefit of this marketing strategy is that it can goes both for short term and long term. Clients that already has been our partners are loyal clients since we handle their projects from beginning until now. ',
    'mark2-1' => 'Long-term partnership with clients',
    'mark2-2' => 'Short-term and long-term marketing',
    'title2' => 'Our Excellence',
  ),
  'footer' => 
  array (
    'socmed' => 'Social Media',
    'navigation' => 'Navigation',
    'contactUs' => 'Contact Us',
    'submitButton' => 'Submit Now',
  ),
  'contactUsTitle' => 'Contact Us',
  'mailUsTitle' => 'Mail Us',
  'banner1' => 
  array (
    'jpg' => 'Sapta Beton',
  ),
  'banner2' => 
  array (
    'jpg' => 'We are here',
  ),
  'banner3' => 
  array (
    'jpg' => 'Your',
  ),
  'subbanner1' => 
  array (
    'jpg' => 'Quality and Trust You Will Remember',
  ),
  'subbanner2' => 
  array (
    'jpg' => 'since 1989',
  ),
  'subbanner3' => 
  array (
    'jpg' => 'Infrastructure solution',
  ),
);
