<?php

return array (
  'websiteName' => 'Sapta Beton',
  'address' => 'Sapta Beton',
  'city' => 'DKI Jakarta',
  'contactUs' => '(021) - 6266325/26 Hunting',
  'mailUs' => 'marketing@saptabeton.com',
  'contactUsTitle' => 'Kontak Kami',
  'cb' => 
  array (
    'title' => 'Latar Belakang Perusahaan',
    'header' => 'Menjadi yang terbaik',
    'subbox1' => 'Akurat',
    'subbox2' => 'Peralatan',
    'subbox3' => 'Fasilitas',
    'subbox4' => 'Kualitas',
    'submenu1' => 'Standart Kualitas',
    'submenu2' => 'Peralatan dan Fasilitas',
    'submenutext1' => 'Melengkapi kebutuhan standart kualitas',
    'submenutext2' => 'Di dukung dengan fasilitas dan peralatan prima',
  ),
  'banner1' => 
  array (
    'jpg' => 'Sapta Beton
',
  ),
  'banner2' => 
  array (
    'jpg' => 'Kami Disini',
  ),
  'banner3' => 
  array (
    'jpg' => 'Solusi infrastuktur',
  ),
  'dev' => 
  array (
    'mark1-1' => 'Lokasi yang strategis',
    'mark1-2' => 'Kualitas yang prima',
    'mark2-1' => 'Hubungan jangka panjang dengan clients',
    'mark2-2' => 'Marketing dalam jangka pendek dan panjang',
  ),
  'subbanner2' => 
  array (
    'jpg' => 'Sejak 1989',
  ),
  'subbanner3' => 
  array (
    'jpg' => 'Anda',
  ),
);
