<!-- //banner -->
@extends('layout.page')
@section('title')
{{trans('career.title')}}
@endsection

@section('content')
<div class="contact_page">
		<div class="container">
			<h3 class="title">{{trans('career.title')}}</h3>
			@if(Session::has('message-career'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message-career') }}</p>
			@endif
			<div class="col-md-8 contact-grids1 animated wow fadeInRight" data-wow-delay=".5s">
				{{ Form::open(['route' => 'career.store', 'files' => true]) }}
						<div class="contact-form2">
							<h4>{{trans('career.name')}}</h4>
							
								<input type="text" name="Name" placeholder="" required="" value="{{ old('Name') }}">
							
						</div>
						@if($errors->has('Name'))
						<span class="error">
		                    <strong>{{ $errors->first('Name') }}</strong>
		                </span>
						@endif
						<div class="contact-form2">
							<h4>{{trans('career.email')}}</h4>
						
								<input type="email" name="Email" placeholder="" required="" value="{{ old('Email') }}">
						
						</div>
						@if($errors->has('Email'))
						<span class="error">
		                    <strong>{{ $errors->first('Email') }}</strong>
		                </span>
						@endif
						<div class="contact-form2">
							<h4>{{trans('career.subject')}}</h4>
						
								<input type="text" name="Subject" placeholder="" required="" value="{{ old('Subject') }}">
						
						</div>
						@if($errors->has('Subject'))
						<span class="error">
		                    <strong>{{ $errors->first('Subject') }}</strong>
		                </span>
						@endif
						<div class="contact-form2">
							<h4>{{trans('career.upload')}} (doc, docx, pdf)</h4>
						
								<input type="file" name="File" placeholder="" required="">
						
						</div>
						@if($errors->has('File'))
						<span class="error">
		                    <strong>{{ $errors->first('File') }}</strong>
		                </span>
						@endif
				<div class="contact-me ">
					<h4>{{trans('career.message')}}</h4>
				
						<textarea name="Message" placeholder="" required="">{{ old('Message') }}</textarea>
						</div>

						{!! app('captcha')->display(); !!}

						<input type="submit" value="Submit" >
				{{ Form::close() }}
				
			</div>
			
			<div class="col-md-4 contact-grids">
				<div class=" contact-grid animated wow fadeInLeft" data-wow-delay=".5s">
					<div class="contact-grid1">
						<div class="contact-grid2 ">
							<i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>
						</div>
						<div class="contact-grid3">
							<h4>{{trans('career.addressTitle')}}</h4>
							<p>{{ trans('home.address') }} <span>{{ trans('home.city') }}</span></p>
						</div>
					</div>
				</div>
				<div class=" contact-grid animated wow fadeInUp" data-wow-delay=".5s">
					<div class="contact-grid1">
						<div class="contact-grid2 contact-grid4">
							<i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>
						</div>
						<div class="contact-grid3">
							<h4>{{trans('home.contactUsTitle')}}</h4>
							<p>{{ trans('home.contactUs') }}</p>
						</div>
					</div>
				</div>
				<div class=" contact-grid animated wow fadeInRight" data-wow-delay=".5s">
					<div class="contact-grid1">
						<div class="contact-grid2 contact-grid5">
							<i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>
						</div>
						<div class="contact-grid3">
							<h4>{{trans('home.mailUsTitle')}}</h4>
							<p><a href="mailto:{{ trans('home.mailUs') }}">{{ trans('home.mailUs') }}</a></p>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
@endsection