@extends('layout.page')
@section('title')
{{trans('gallery.title')}}
@endsection

	@push('scripts')
	<link rel="stylesheet" href="{{ asset('css/swipebox.css') }}">
	<script src="{{ asset('js/responsiveslides.min.js') }}"></script>
    <!-- Include jQuery & Filterizr -->
    <script src="{{ asset('js/jquery.filterizr.js') }}"></script>
    <script src="{{ asset('js/controls.js') }}"></script>
    <!-- Kick off Filterizr -->
    <script type="text/javascript">
        $(function() {
            //Initialize filterizr with default options
            $('.filtr-container').filterizr();
        });
    </script>
	<!--//gallery-->
<!-- swipe box js -->
	<script src="{{ asset('js/jquery.swipebox.min.js') }}"></script> 
	    <script type="text/javascript">
			jQuery(function($) {
				$(".swipebox").swipebox();
			});
	</script>
<!-- //swipe box js -->
	@endpush

	
@section('content')
<div class="gallery">
	<div class="container">
		<h3 class="title"> {{trans('gallery.title')}} </h3>
		<div class="gallery_gds">
			<ul class="simplefilter ">
                <li class="active" data-filter="all">All</li>
                @foreach($categories as $category)
                <li data-filter="{{$category->ProductsCategoryId}}">{{$category->CategoryName}}</li>
                @endforeach
            </ul>   
            <div class="filtr-container ">
            @foreach($products as $product)
                <div class=" col-md-4 filtr-item" data-category="{{$product->ProductsCategoryId}}" data-sort="{{$product->MediaName}}">
					<a href="{{ asset('images/gallery/'.$product->FileName) }}" class="b-link-stripe b-animate-go  swipebox">
						<div class="item item-type-double">
							<div class="item-hover">
								<div class="item-info">
									<div class="date">{{$product->MediaName}}</div>			
									<div class="line"></div>			
									<div class="headline">{{$product->Description}}</div>
									<div class="line"></div>
								</div>
								<div class="mask"></div>
							</div>
							<div class="item-img"><img src="{{ asset('images/gallery/'.$product->FileName) }}" alt=" " /></div>
						</div>
					</a>
                </div>
            @endforeach
               <div class="clearfix"> </div>
            </div>
		</div>
	</div>	
</div>
@endsection