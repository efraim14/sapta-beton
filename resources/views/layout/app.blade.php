<!DOCTYPE html>
<html>
<head>
	@include('layout.includes.link')
</head>
<body>
@include('layout.includes.header')

<div class="body-wrapper">
	
@include('layout.includes.banner')

@yield('content')

@include('layout.includes.footer')
</div>
</body>
</html>