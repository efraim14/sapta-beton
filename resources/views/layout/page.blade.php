<!DOCTYPE html>
<html>
<head>
	@include('layout.includes.link')

	@stack('scripts')
</head>
<body>
	@include('layout.includes.header')
<div class="body-wrapper">
	
	@include('layout.includes.pagebanner')

@yield('content')

@include('layout.includes.footer')
</div>
</body>
</html>