<!-- contact -->
<div class="contact">
	<div class="container">
		<div class="col-md-4 contact-left">
			<h2><a href="index.html">{{trans('home.footer.socmed')}}</a></h2>
			<hr />
			<ul class="fb_icons2">
				<li><a class="fb" href="#"></a></li>
				<li><a class="twit" href="#"></a></li>
				<li><a class="goog" href="#"></a></li>
				<li><a class="pin" href="#"></a></li>
				<li><a class="drib" href="#"></a></li>
			</ul>
		</div>
		<div class="col-md-3 contact-mid">
			<h3>{{trans('home.footer.navigation')}}</h3>
			<ul>
				@foreach($menuNavbar as $menus)
				<li><a href="{{$menus->MenuController}}"><span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>{{ $menus->MenuName }}</a></li>
				@endforeach
			</ul>
		</div>
		<div class="col-md-5 mail_right">
			<h3>{{trans('home.footer.contactUs')}}</h3>
			@if(Session::has('message'))
				<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
			@endif
				{{ Form::open(['route' => 'index.store']) }}
					<input type="text" name="Name" value="Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}" required="" value="{{ old('Name') }}">
					@if($errors->has('Name'))
						<span class="error">
		                    <strong>{{ $errors->first('Name') }}</strong>
		                </span>
					@endif
					<input type="email" name="Email" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="" value="{{ old('Email') }}">
					@if($errors->has('Email'))
						<span class="error">
		                    <strong>{{ $errors->first('Email') }}</strong>
		                </span>
					@endif
					<textarea name="Message"onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message...';}" required="">{{ old('Message') }}</textarea>
					{!! app('captcha')->display(); !!}
					<input type="submit" value="{{trans('home.footer.submitButton')}}">
				{{ Form::close() }}
		</div>
		<a id="contact">
		</a>
		<div class="clearfix"></div>
		<p class="footer">&copy; 2016 {{ trans('home.websiteName') }}. All Rights Reserved | W3L</p>
	</div>
</div>
<!-- //contact -->