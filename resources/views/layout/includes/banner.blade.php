<!-- banner -->
<div class="banner">
				<script>
						// You can also use "$(window).load(function() {"
						$(function () {
						 // Slideshow 4
						$("#slider3").responsiveSlides({
							auto: true,
							pager: false,
							nav: false,
							speed: 500,
							namespace: "callbacks",
							before: function () {
						$('.events').append("<li>before event fired.</li>");
						},
						after: function () {
							$('.events').append("<li>after event fired.</li>");
							}
							});
						});
				</script>

		<div  id="top" class="callbacks_container">
			<ul class="rslides" id="slider3">
			@foreach ($banners as $banner)
				<li>
					<div class="banner1" style="background-image: url({{ asset('images/'.$banner->Image) }});">
						<div class="container">
							<div class="banner-info">
								<h3><span> {{trans('home.'.$banner->Image)}} </span></h3>
								<p>{{trans('home.sub'.$banner->Image)}}</p>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</li> 
			@endforeach
			</ul>
		</div>
		<div class="clearfix"></div>
</div>
<!-- //banner -->