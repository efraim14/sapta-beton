<title>@yield('title') | {{ trans('home.websiteName') }}</title>
<!--header-->
<div class="ban-top">
		<div class="top_nav_left">
			<nav class="navbar navbar-default">
			  <div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse menu--shylock" id="bs-example-navbar-collapse-1">
				  <ul class="nav navbar-nav menu__list">
					@foreach($menuNavbar as $menus)
					<li class="menu__item {{ $currentURL==$menus->MenuController ? 'active menu__item--current' : ''}}"><a class="menu__link" href="{{$menus->MenuController}}">{{ $menus->MenuName }}</a></li>
					@endforeach
				  </ul>
				</div>
			  </div>
			</nav>	
		</div>

		<div class="language-icon">
		<a href="{{url('/en')}}"><img src="{{asset('images/en.png')}}" /></a>
		<a href="{{url('/id')}}"><img src="{{asset('images/id.png')}}" /></a>
		</div>
		<div class="clearfix"></div>
</div>
<!-- //header -->
<!-- banner-bot -->
<div class="header-bot">
	<div class="container">
		<div class="col-md-5 header_left">
			<h1><a href="{{ URL::to('/') }}"><img src="{{ asset('images/saptalogo.png') }}"/></a></h1>
		</div>
		<div class="col-md-7 header_right">
			<ul class="contactList">
				<li><i class="glyphicon glyphicon-phone"></i>{{ trans('home.contactUsTitle') }}<span><a href="tel:{{ trans('home.contactUs') }}">{{ trans('home.contactUs') }}</a></span></li>
				<li><i class="glyphicon glyphicon-envelope"></i>{{ trans('home.mailUsTitle') }}<span><a href="mailto:{{ trans('home.mailUs') }}">{{ trans('home.mailUs') }}</a></span></li>
			</ul>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<!-- //banner-bot -->