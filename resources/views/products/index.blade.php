@extends('layout.page')
@section('title')
{{trans('products.title')}}
@endsection

@push('scripts')
<script src="{{asset('js/responsiveslides.min.js')}}"></script>
    <script src="{{asset('js/easyResponsiveTabs.js')}}"></script>
	<script type="text/javascript">
    $(document).ready(function() {
        //Vertical Tab
        $('#parentVerticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo2');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });
    });
</script>
@endpush

@section('content')
<div class="about_page">
    <div class="container">
        <h3 class="title" > {{trans('products.title')}} </h3>
        <div id="parentVerticalTab">
            <ul class="resp-tabs-list hor_1">
                <li>{{trans('products.tab1')}}</li>
                <li>{{trans('products.tab2')}}</li>
                <li>{{trans('products.tab3')}}</li>
                <li>{{trans('products.tab4')}}</li>
            </ul>
            <div class="resp-tabs-container hor_1">
                <div>
                    <h4>{{trans('products.tab1')}}</h4>
                    <div class="col-md-6 easy-left">
                        <img src="{{asset('images/readymix.jpg')}}" alt=" "/>
                    </div>
                    <div class="clearfix"></div>
                        <p>{{trans('products.tabdesc1')}}</p>
                </div>
                <div>
                    <h4>{{trans('products.tab2')}}</h4>
                    <div class="col-md-6 easy-left">
                        <img src="{{asset('images/lab.jpg')}}" alt=" "/>
                    </div>
                    <div class="clearfix"></div>
                        <p>{{trans('products.tabdesc2')}}</p>
                </div>
                <div>
                    <h4>{{trans('products.tab3')}}</h4>
                    <div class="col-md-6 easy-left">
                        <img src="{{asset('images/timbangan.jpg')}}" alt=" "/>
                    </div>
                    <div class="clearfix"></div>
                        <p>{{trans('products.tabdesc3')}}</p>
                </div>
                <div>
                    <h4>{{trans('products.tab4')}}</h4>
                    <div class="col-md-6 easy-right">
                        <ul>
                            <li><a href="#"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>Concrete Pump</a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>Vibrator</a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>Spooring</a></li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                        <p>{{trans('products.tabdesc4')}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- team -->
<div class="team-page">
    <div class="container">
        <h3 class="title">{{trans('products.clients')}}</h3>
        <ul class="list-group">
        @foreach ($services as $service)
              <li class="list-group-item"><h4>{{$service->Name}}</h4>
                <p>{{$service->ShortDesc}}</p></li>
        @endforeach
        </ul>
        <div class="clearfix"></div>
    </div>
</div>
@endsection