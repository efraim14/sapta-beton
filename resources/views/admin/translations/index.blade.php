@extends('admin.layout.app')

@section('title')
Translations
@endsection

@section('content')
<iframe src="{{URL::to('/translations')}}" height="500px" width="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
@endsection