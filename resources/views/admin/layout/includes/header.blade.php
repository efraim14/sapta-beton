<title>@yield('title') | Admin - {{ trans('home.websiteName') }}</title>

<!--header start here-->
<div class="header-main">
	<div class="logo-w3-agile">
		<h1><a href="{{ URL::to('/administrator') }}"><img src="{{ asset('images/saptalogo.png') }}"/></a></h1>
	</div>

	<div class="profile_details w3l">		
		<ul>
			<li class="dropdown profile_details_drop">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
					<div class="profile_img">	
						<div class="user-name">
							<p>{{ $user }}</p>
							<span>Administrator</span>
						</div>
						<i class="fa fa-angle-down"></i>
						<i class="fa fa-angle-up"></i>
						<div class="clearfix"></div>	
					</div>	
				</a>
				<ul class="dropdown-menu drp-mnu">
					<li> <a href="{{--route('profile')--}}"><i class="fa fa-user"></i> Profile</a> </li> 
					<li> <a href="{{route('logout')}}"><i class="fa fa-sign-out"></i> Logout</a> </li>
				</ul>
			</li>
		</ul>
	</div>

	<div class="clearfix"> </div>	
</div>
<!--heder end here-->


<ol class="breadcrumb">
	<li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a> <i class="fa fa-angle-right"></i>
	@if ($currentURL != '')
	<a href="{{URL::to($currentURL)}}">@yield('title')</a> 
	@endif
	</li>
</ol>