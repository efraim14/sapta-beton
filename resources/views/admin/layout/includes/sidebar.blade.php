<!--/sidebar-menu-->
<div class="sidebar-menu">
	<header class="logo1">
		<a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a> 
	</header>
	<div style="border-top:1px ridge rgba(255, 255, 255, 0.15)"></div>
	<div class="menu">
		<ul id="menu" >

			<li><a href="{{route('admin')}}"><i class="fa fa-tachometer"></i> <span>Dashboard</span><div class="clearfix"></div></a></li>
			<li><a href="{{route('translations')}}"><i class="fa fa-file-text-o"></i> <span>Translations</span> <div class="clearfix"></div></a></li>
			<li><a href="{{route('forms')}}"><i class="fa fa-clone"></i> <span>Forms</span><div class="clearfix"></div></a></li>
			<li><a href="{{route('clients.index')}}"><i class="fa fa-briefcase"></i> <span>Clients</span><div class="clearfix"></div></a></li>
			<li><a href="{{route('gallery.index')}}"><i class="fa fa-object-group"></i> <span>Gallery</span><div class="clearfix"></div></a></li>
			<li><a href="{{URL::to('/register')}}"><i class="fa fa-user"></i> <span>Register</span><div class="clearfix"></div></a></li>
			<li><a href="{{route('home')}}" target="_blank"><i class="fa fa-external-link"></i> <span>Visit Home</span><div class="clearfix"></div></a></li>
		</ul>
	</div>
</div>
<div class="clearfix"></div>		