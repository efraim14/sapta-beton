<!DOCTYPE HTML>
<html>
<head>
	@include('admin.layout.includes.link')
	 @stack('scripts')
</head> 
<body>
	<div class="page-container">
		<!--/content-inner-->
		<div class="left-content">
			<div class="mother-grid-inner">
				
				@include('admin.layout.includes.header')

				@yield('content')

				@include('admin.layout.includes.footer')

			</div>
		</div>
		<!--//content-inner-->
		@include('admin.layout.includes.sidebar')
	</div>

@include('admin.layout.includes.js')

</body>
</html>