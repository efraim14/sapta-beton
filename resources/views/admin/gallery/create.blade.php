@extends('admin.layout.app')
@section('title')
Create Gallery
@endsection

@section('content')
<div class="grid-form1">
      @if($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    
 		<h2 id="forms-example" class="">Create Gallery</h2>

{{ Form::open(['route' => 'gallery.store', 'files' => true]) }}
 <div class="form-group">
    {!! Form::label('title', 'Name', ['class' => 'control-label']) !!}
    {!! Form::text('MediaName', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('title', 'Image (JPG, PNG)', ['class' => 'control-label']) !!}
    {!! Form::file('FileName') !!}
</div>
<div class="form-group">
    {!! Form::label('title', 'Category', ['class' => 'control-label']) !!}
    <select name="ProductsCategoryId" class="form-control" onchange="CheckColors(this.value)";>
    <option value="" disabled selected>--Choose Category--</option>
    <option value="New">New Category</option>
    @foreach ($categories as $category)
      <option value="{{$category->ProductsCategoryId}}">{{$category->CategoryName}}</option>
    @endforeach
    </select>
</div>
<div class="form-group" id="NewCategory" style="display:none;">
    {!! Form::label('title', 'New Category Name', ['class' => 'control-label']) !!}
    {!! Form::text('NewCategoryName', null, ['class' => 'form-control']) !!}
</div>
  {{Form::submit('Submit',['class'=>'btn btn-default form-control'])}}
{{ Form::close() }}

</div>
@endsection

@push('scripts')
<script type="text/javascript">
function CheckColors(val){
 var element=document.getElementById('NewCategory');
 if(val=='New')
   element.style.display='block';
 else  
   element.style.display='none';
}
</script> 
@endpush