@extends('admin.layout.app')


@section('title')
Gallery
@endsection

@section('content')
@if(Session::has('message'))
  <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
@endif
<div class="bs-component mb20" style="float:right; width: 20%">
   <a href="{{route('gallery.create')}}"><button type="button" class="btn btn-primary btn-block">Create</button></a>
</div>
<div class="bs-component mb20" style="float:left; width: 20%">
      <a href="{{route('category.index')}}"><button type="button" class="btn btn-primary btn-block">Go to Categories</button></a>
</div>
<table id="table" class="table table-striped table-bordered" width="100%" cellspacing="0">
<thead>
	<tr>
	  <th>ID</th>
		<th>Name</th>
		<th>File Name</th>
    <th>Category</th>
    <th class="Edit"></th>
    <th class="Delete"></th>
	</tr>
</thead>
</table>
@endsection


@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
    $('#table').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "{{route('getGallery')}}",
        "order": [[ 0, "desc" ]],
        "columns": [
            { data: 'ProductsId', name: 'ID' },
            { data: 'MediaName', name: 'Name' },
            { data: 'FileName', name: 'File Name' },
            { data: 'category.CategoryName', name: 'Category'},
            { data: 'edit', name: 'Edit'},
            { data: 'delete', name: 'Delete'}
        ],
      "columnDefs": [
      {
        data: 'FileName',
        searchable: false,
        "render": function(data, type, row) { // Available data available for you within the row
        return "<a href='{{asset('/images/gallery')}}/"+data+"' target='_blank'>"+data+"</a>";
        },
        "targets" : 2
      }
      ]
    } );
} );
</script>

<script>
      function ConfirmDelete()
      {
      var x = confirm("Are you sure you want to delete?");
      if (x)
        return true;
      else
        return false;
      }
</script>
@endpush
