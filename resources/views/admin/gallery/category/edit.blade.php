@extends('admin.layout.app')
@section('title')
Edit Category
@endsection

@section('content')
<div class="grid-form1">
    @if($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    
 		<h2 id="forms-example" class="">Edit Category</h2>

{{ Form::model($data, [
    'method' => 'PATCH',
    'route' => ['category.update', $data->ProductsCategoryId],
    'files' => true
]) }}
<div class="form-group">
    {!! Form::label('title', 'Category Name', ['class' => 'control-label']) !!}
    {!! Form::text('CategoryName', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('title', 'Description', ['class' => 'control-label']) !!}
    {!! Form::textarea('Description', null, ['class' => 'form-control']) !!}
</div>
  {{Form::submit('Submit',['class'=>'btn btn-default form-control'])}}
{{ Form::close() }}

</div>
@endsection