@extends('admin.layout.app')


@section('title')
Gallery's Category
@endsection

@section('content')
@if(Session::has('message'))
  <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
@endif
<table id="table" class="table table-striped table-bordered" width="100%" cellspacing="0">
<thead>
	<tr>
	  <th>ID</th>
		<th>Category Name</th>
		<th>Description</th>
    <th class="Edit"></th>
    <th class="Delete"></th>
	</tr>
</thead>
</table>
@endsection


@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
    $('#table').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "{{route('getGalleryCategory')}}",
        "order": [[ 0, "desc" ]],
        "columns": [
            { data: 'ProductsCategoryId', name: 'ID' },
            { data: 'CategoryName', name: 'Category Name' },
            { data: 'Description', name: 'Description' },
            { data: 'edit', name: 'Edit'},
            { data: 'delete', name: 'Delete'}
        ]
    } );
} );
</script>

<script>
      function ConfirmDelete()
      {
      var x = confirm("Are you sure you want to delete?");
      if (x)
        return true;
      else
        return false;
      }
</script>
@endpush
