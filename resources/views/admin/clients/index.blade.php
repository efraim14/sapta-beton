@extends('admin.layout.app')


@section('title')
Clients
@endsection

@section('content')
@if(Session::has('message'))
  <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
@endif
<div class="bs-component mb20" style="float:right; width: 20%">
   <a href="{{route('clients.create')}}"><button type="button" class="btn btn-primary btn-block">Create</button></a>
</div>
<table id="table" class="table table-striped table-bordered" width="100%" cellspacing="0">
<thead>
	<tr>
	  <th>ID</th>
		<th>Name</th>
		<th>Short Description</th>
    <th>Description</th>
    <th class="Edit"></th>
    <th class="Delete"></th>
	</tr>
</thead>
</table>
@endsection


@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
    $('#table').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "{{route('getServices')}}",
        "order": [[ 0, "desc" ]],
        "columns": [
            { data: 'ServicesId', name: 'ID' },
            { data: 'Name', name: 'Name' },
            { data: 'ShortDesc', name: 'Short Description' },
            { data: 'Description', name: 'Description'},
            { data: 'edit', name: 'Edit'},
            { data: 'delete', name: 'Delete'}
        ]
    } );
} );
</script>

<script>
      function ConfirmDelete()
      {
      var x = confirm("Are you sure you want to delete?");
      if (x)
        return true;
      else
        return false;
      }
</script>
@endpush
