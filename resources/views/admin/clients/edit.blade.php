@extends('admin.layout.app')
@section('title')
Edit Clients
@endsection

@section('content')
<div class="grid-form1">
    @if($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    
 		<h2 id="forms-example" class="">Edit Clients</h2>

{{ Form::model($data, [
    'method' => 'PATCH',
    'route' => ['clients.update', $data->ServicesId]
]) }}

<div class="form-group">
    {!! Form::label('Name', 'Name', ['class' => 'control-label']) !!}
    {!! Form::text('Name', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('title', 'ShortDesc', ['class' => 'control-label']) !!}
    {!! Form::text('ShortDesc', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('title', 'Description', ['class' => 'control-label']) !!}
    {!! Form::textarea('Description', null, ['class' => 'form-control']) !!}
</div>

  {{Form::submit('Submit',['class'=>'btn btn-default form-control'])}}
{{ Form::close() }}

</div>
@endsection