@extends('admin.layout.app')


@section('title')
Forms
@endsection

@section('content')
<table id="table" class="table table-striped table-bordered" width="100%" cellspacing="0">
<thead>
	<tr>
	    <th>ID</th>
		<th>Name</th>
		<th>Email</th>
        <th>Form Type</th>
		<th>Message</th>
		<th>Attachment</th>
		<th>Submitted At</th>
	</tr>
</thead>
</table>
@endsection


@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
    $('#table').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "{{route('getForms')}}",
        "order": [[ 0, "desc" ]],
        "columns": [
            { data: 'FormId', name: 'ID' },
            { data: 'Name', name: 'Name' },
            { data: 'Email', name: 'Email' },
            { data: 'Type', name: 'Form Type'},
            { data: 'Message', name: 'Message' },
            { data: 'Attachment', name: 'Attachment' },
            { data: 'created_at', name: 'Submitted At' }
        ],
        "columnDefs": [
            {
              data: "Type",
              "render": function(data, type, row) { // Available data available for you within the row
                if (data == 0) {
                    return 'Contact Us';
                } else {
                    return 'Career';
                }
              },
              "targets" : 3
            },
            {
              data: "Attachment",
              "render": function(data, type, row) { // Available data available for you within the row
                if (data != null) {
                    return "<a href='{{asset('/file_cv')}}/"+data+"'>Download</a>";
                } else {
                    return '';
                }
              },
              "targets" : 5
            }
          ]
    } );
} );
</script>
@endpush
