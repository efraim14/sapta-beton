@extends('layout.app')
@section('title')
Home
@endsection

@section('content')
<!-- content -->
<div class="content">
	<div class="container">
		<h3>{{trans('home.cb.title')}}</h3>
		<div class="col-md-7 content-left">
			<h4>{{trans('home.cb.header')}} <span>{{trans('home.cb.subheader')}}</span></h4>
			<p class="abt-para">{{trans('home.cb.text')}}</p>
			<div class="col-md-6 bann-strip-grid">
			   	  <div class="col-md-8 b-strip-right">
			   	  	 <h5>{{trans('home.cb.submenu1')}}</h5>
			   	  	 <p>{{trans('home.cb.submenutext1')}}</p>
			   	  </div>
				  <div class="col-md-4 b-strip-left">
			   	  	 <span class="glyphicon glyphicon-leaf hovicon effect-4 sub-b" aria-hidden="true"> </span>
			   	  </div>
			   	  <div class="clearfix"> </div>
			</div>
			<div class="col-md-6 bann-strip-grid">
			   	  <div class="col-md-8 b-strip-right">
			   	  	 <h5>{{trans('home.cb.submenu2')}}</h5>
			   	  	 <p>{{trans('home.cb.submenutext2')}}</p>
			   	  </div>
				  <div class="col-md-4 b-strip-left">
			   	  	 <span class="glyphicon glyphicon-fire hovicon effect-4 sub-b" aria-hidden="true"> </span>
			   	  </div>
			   	  <div class="clearfix"> </div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="col-md-5 content-right">
			<div class="con-grids">
				<div class="con-lt con-lt1 text-center">
					<span class="glyphicon glyphicon-ok icon" aria-hidden="true"></span>
					<h4>{{trans('home.cb.subbox1')}} </h4>
					<p>{{trans('home.cb.subboxtext1')}}</p>
				</div>
				<div class="con-lt con-lt2 text-center">
					<span class="glyphicon glyphicon-cog icon" aria-hidden="true"></span>
					<h4>{{trans('home.cb.subbox2')}} </h4>
					<p>{{trans('home.cb.subboxtext2')}}</p>
				</div>
			</div>
			<div class="con-grids">
				<div class="con-lt con-lt3 text-center">
					<span class="glyphicon glyphicon-road icon" aria-hidden="true"></span>
					<h4>{{trans('home.cb.subbox3')}} </h4>
					<p>{{trans('home.cb.subboxtext3')}}</p>
				</div>
				<div class="con-lt con-lt4 text-center">
					<span class="glyphicon glyphicon-leaf icon" aria-hidden="true"></span>
					<h4>{{trans('home.cb.subbox4')}} </h4>
					<p>{{trans('home.cb.subboxtext4')}}</p>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<!-- //content -->
<!-- quality -->
<div class="quality">
	<div class="container">
				<h3>{{trans('home.dev.title')}}</h3>
			<div class="col-md-6 col-news-right ">
				<div class="col-news-top">
					<div class="date-in">
						<img class="img-responsive" src="{{ asset('images/marketing.jpg') }}" alt="">
						<div class="month-in">
							<span><img src="{{ asset('images/icon1.png') }}" alt=" "></span>
							<p>{{trans('home.dev.title1')}}</p>
						</div>
					</div>
					<div class="col-bottom two">
						<h4>{{trans('home.dev.subtitle1')}}</h4>
						<p>{{trans('home.dev.subtext1')}} </p>	
							<ul>
								<li><a href="#"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>{{trans('home.dev.mark1-1')}}</a></li>
								<li><a href="#"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>{{trans('home.dev.mark1-2')}}</a></li>
							</ul>
					</div>
				</div>	
						
			</div>
			<div class="col-md-6 col-news ">
						<div class="col-bottom two">
							<h4>{{trans('home.dev.subtitle2')}}</h4>
							<p>{{trans('home.dev.subtext2')}} </p>	
							<ul>
								<li><a href="#"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>{{trans('home.dev.mark2-1')}}</a></li>
								<li><a href="#"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>{{trans('home.dev.mark2-2')}}</a></li>
							</ul>
						</div>
						<div class="col-news-top">
							<div class="date-in">
								<img class="img-responsive" src="{{ asset('images/excellence.jpg') }}" alt="">
								<div class="month-in">
									<span><img src="{{ asset('images/icon2.png') }}" alt=" "></span>
									<p>{{trans('home.dev.title2')}}</p>									
								</div>
							</div>
						</div>		
			</div>
			<div class="clearfix"> </div>
	</div>
</div>
<!-- //quality -->
@endsection