<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblAAMMenu extends Model
{
    protected $table='tblAAMMenu';
    protected $primaryKey='MenuId';
    protected $fillable = ['MenuName', 'MenuController', 'Description', 'IsActive'];
}
