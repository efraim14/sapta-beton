<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblAAMConfig extends Model
{
    protected $table='tblAAMConfig';
    protected $primaryKey='ConfigId';
    protected $fillable = ['ConfigName', 'Value', 'IsActive'];
}
