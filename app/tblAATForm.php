<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblAATForm extends Model
{
    protected $table='tblAATForm';
    protected $primaryKey='FormId';
    protected $fillable = ['Name', 'Email', 'Type', 'Message', 'Attachment'];
}
