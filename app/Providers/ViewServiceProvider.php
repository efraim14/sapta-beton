<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;

use App\TblAAMConfig;
use App\TblAAMMenu;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        
        //Navbar dan Title Bar
        view()->composer('*', function($view)
        {
           $this->getAllData($view);
        });

    }

    private function getAllData($view){
            $data=array();
            $data['currentURL'] = Route::getFacadeRoot()->current()->uri();
            $data['menuNavbar'] = TblAAMMenu::where('IsActive', 1)->get();
            $data['background'] = TblAAMConfig::where('ConfigName', 'BackgroundName')->first();
            $view->with($data);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
