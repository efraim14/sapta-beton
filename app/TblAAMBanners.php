<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblAAMBanners extends Model
{
    protected $table='tblAAMBanners';
    protected $primaryKey='BannerId';
    protected $fillable = ['Name', 'Image', 'Description', 'SubDescription', 'IsActive'];
}
