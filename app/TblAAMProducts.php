<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblAAMProducts extends Model
{
    protected $table='tblAAMProducts';
    protected $primaryKey='ProductsId';
    protected $fillable = ['MediaName', 'FileName', 'ProductsCategoryId', 'IsActive'];

    public function category() {
        return $this->belongsTo('App\TblAAMProductsCategory', 'ProductsCategoryId', 'ProductsCategoryId');
    }
}
