<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblAAMServices extends Model
{
    protected $table='tblAAMServices';
    protected $primaryKey='ServicesId';
    protected $fillable = ['Name', 'FileName', 'ShortDesc', 'Description', 'IsActive'];
}
