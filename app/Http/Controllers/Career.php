<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\TblAATForm;

use Session;

use Input;

use File;

class Career extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('career.index');
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
            'Name' => 'required|max:255',
            'Email' => 'required|email',
            'Subject' => 'required',
            'File'   => 'required|mimes:doc,docx,pdf',
            'g-recaptcha-response' => 'required|captcha'
        ]);
        $input = $request->all();
        $input['Type'] = 1;

        $table = TblAATForm::create($input);

    	$file = Input::file('File');

        $filename = $input['Email'].'-'.$table->FormId.'.'.$file->getClientOriginalExtension();

        $destinationPath =public_path().'/file_cv';

        $upload_success = $file->move($destinationPath, $filename);

        $table['Attachment'] = $filename;

        $table->save();

        Session::flash('message-career', 'Your CV successfully submitted! We will reply your email soon.'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect(url()->previous())->withInput();

    }
}
