<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Auth;
use Session;
use Input;
use File;

use App\TblAAMProducts;
use App\TblAAMProductsCategory;

class AdminGallery extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function userName() {
        $data['user'] = Auth::user()->name;
        return $data;
    }

    public function index()
    {
        return view('admin.gallery.index')->with($this->userName());
    }

    public function getGallery()
    {
        $data = TblAAMProducts::with('category')->get();
        return Datatables::of($data)->addColumn('edit', function ($data) {
            return '<a href="'.route('gallery.edit', $data->ProductsId).'" class="btn btn-success"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
        })->addColumn('delete', function ($data) {
            return '<form role="form" method="post" onsubmit="return ConfirmDelete()" action="'.route('gallery.destroy',$data->ProductsId).'"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="'.csrf_token().'"><button type="submit" class="btn btn-danger" name="delete">Delete</button></form>';
        })->make(true); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = array();
        $data['categories'] = TblAAMProductsCategory::where('IsActive', true)->get();
        $data['user'] = Auth::user()->name;
        return view('admin.gallery.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'MediaName' => 'required|max:255',
            'FileName' => 'required|image',
            'ProductsCategoryId' => 'required'
        ]);
        $input['MediaName'] = $request->MediaName;
        if ($request->ProductsCategoryId == 'New') {
            $input2['CategoryName'] = $request->NewCategoryName;
            $tblAAMProductsCategory = TblAAMProductsCategory::create($input2);
            $input['ProductsCategoryId'] = $tblAAMProductsCategory->ProductsCategoryId;
        } else {
            $input['ProductsCategoryId'] = $request->ProductsCategoryId;
        }

        $table = TblAAMProducts::create($input);
        $file = Input::file('FileName');
        $filename = $table->ProductsId.'.'.$file->getClientOriginalExtension();
        $destinationPath =public_path().'/images/gallery';
        $upload_success = $file->move($destinationPath, $filename);
        $table['FileName'] = $filename;
        $table->save();

        Session::flash('message', 'Gallery successfully created.'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->route('gallery.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = array();
        $data['categories'] = TblAAMProductsCategory::where('IsActive', true)->get();
        $data['data'] =  TblAAMProducts::findOrFail($id);
        $data['user'] = Auth::user()->name;
        return view('admin.gallery.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data =  TblAAMProducts::findOrFail($id);
         $this->validate($request, [
            'MediaName' => 'required|max:255',
            'FileName' => 'image',
            'ProductsCategoryId' => 'required'
        ]);
        $input = $request->all();

        $input['MediaName'] = $request->MediaName;
        if ($request->ProductsCategoryId == 'New') {
            $input2['CategoryName'] = $request->NewCategoryName;
            $tblAAMProductsCategory = TblAAMProductsCategory::create($input2);
            $input['ProductsCategoryId'] = $tblAAMProductsCategory->ProductsCategoryId;
        } else {
            $input['ProductsCategoryId'] = $request->ProductsCategoryId;
        }

        $file = Input::file('FileName');
        if ($file !== null) {
            $filename = $data->ProductsId.'.'.$file->getClientOriginalExtension();
            $destinationPath =public_path().'/images/gallery';
            $upload_success = $file->move($destinationPath, $filename);
            $input['FileName'] = $filename;
        }
        $data->fill($input)->save();

        Session::flash('message', 'Gallery successfully edited.'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->route('gallery.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data =  TblAAMProducts::findOrFail($id);

        $data->delete();
        Session::flash('message', 'Gallery successfully deleted.'); 
        Session::flash('alert-class', 'alert-danger'); 
        return redirect()->route('gallery.index');
        
    }
}
