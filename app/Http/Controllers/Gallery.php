<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\TblAAMProducts;
use App\TblAAMProductsCategory;

class Gallery extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=array();
        $data['products'] = TblAAMProducts::get();
        $data['categories'] = TblAAMProductsCategory::get();
        return view('gallery.index')->with($data);
    }

}
