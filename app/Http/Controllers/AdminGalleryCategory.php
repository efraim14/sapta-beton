<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Auth;
use Session;

use App\TblAAMProductsCategory;

class AdminGalleryCategory extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function userName() {
        $data['user'] = Auth::user()->name;
        return $data;
    }

    public function index()
    {
        return view('admin.gallery.category.index')->with($this->userName());
    }

    public function getGalleryCategory()
    {
        $data = TblAAMProductsCategory::get();
        return Datatables::of($data)->addColumn('edit', function ($data) {
            return '<a href="'.route('category.edit', $data->ProductsCategoryId).'" class="btn btn-success"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
        })->addColumn('delete', function ($data) {
            return '<form role="form" method="post" onsubmit="return ConfirmDelete()" action="'.route('category.destroy',$data->ProductsCategoryId).'"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="'.csrf_token().'"><button type="submit" class="btn btn-danger" name="delete">Delete</button></form>';
        })->make(true); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = array();
        $data['data'] =  TblAAMProductsCategory::findOrFail($id);
        $data['user'] = Auth::user()->name;
        return view('admin.gallery.category.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data =  TblAAMProductsCategory::findOrFail($id);
         $this->validate($request, [
            'CategoryName' => 'required|max:255',
            'Description' => 'required'
        ]);
        $input = $request->all();

        $data->fill($input)->save();
        Session::flash('message', 'Category successfully edited.'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data =  TblAAMProductsCategory::findOrFail($id);

        $data->delete();
        Session::flash('message', 'Gallery successfully deleted.'); 
        Session::flash('alert-class', 'alert-danger'); 
        return redirect()->route('category.index');
        
    }
}
