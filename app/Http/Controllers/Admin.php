<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Facades\Datatables;

use App\TblAATForm;

class Admin extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function userName() {
		$data['user'] = Auth::user()->name;
		return $data;
	}

	public function index()
	{
		return view('admin.index')->with($this->userName());
	}

	public function translations()
	{
		return view('admin.translations.index')->with($this->userName());
	}

	public function forms()
	{
		return view('admin.forms.index')->with($this->userName());
	}

	public function getForms()
	{
		$data = TblAATForm::all();

        return Datatables::of($data)->make(true); 
	}

	public function logout()
	{
		Auth::logout();
		return redirect()->route('home');
	}

}