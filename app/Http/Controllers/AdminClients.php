<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Auth;
use Session;

use App\TblAAMServices;

class AdminClients extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function userName() {
        $data['user'] = Auth::user()->name;
        return $data;
    }

    public function index()
    {
        return view('admin.clients.index')->with($this->userName());
    }

    public function getServices()
    {
        $data = TblAAMServices::all();

        return Datatables::of($data)->addColumn('edit', function ($data) {
            return '<a href="'.route('clients.edit', $data->ServicesId).'" class="btn btn-success"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
        })->addColumn('delete', function ($data) {
            return '<form role="form" method="post" onsubmit="return ConfirmDelete()" action="'.route('clients.destroy',$data->ServicesId).'"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="'.csrf_token().'"><button type="submit" class="btn btn-danger" name="delete">Delete</button></form>';
        })->make(true); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.clients.create')->with($this->userName());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'Name' => 'required|max:255',
            'ShortDesc' => 'required',
            'Description' => 'required'
        ]);
        $input = $request->all();

        TblAAMServices::create($input);
        Session::flash('message', 'Client successfully created.'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->route('clients.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = array();
        $data['data'] =  TblAAMServices::findOrFail($id);
        $data['user'] = Auth::user()->name;
        return view('admin.clients.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data =  TblAAMServices::findOrFail($id);
        $this->validate($request, [
            'Name' => 'required|max:255',
            'ShortDesc' => 'required',
            'Description' => 'required'
        ]);
        $input = $request->all();

        $data->fill($input)->save();
        Session::flash('message', 'Client successfully edited.'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->route('clients.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data =  TblAAMServices::findOrFail($id);

        $data->delete();
        Session::flash('message', 'Client successfully deleted.'); 
        Session::flash('alert-class', 'alert-danger'); 
        return redirect()->route('clients.index');
        
    }
}
