<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\TblAAMBanners;
use App\TblAATForm;

use Session;

class Index extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function home() {
    	return redirect()->route('home');
    }

    public function index()
    {
    	$data = array();
    	$data['banners'] = TblAAMBanners::get();
        return view('index')->with($data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'Name' => 'required|max:255',
            'Email' => 'required|email',
            'Message' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ]);
        $input = $request->all();
        $input['Type'] = 0;

        TblAATForm::create($input);
        Session::flash('message', 'Your form successfully submitted! We will reply to you soon.'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect(url()->previous() . '#contact')->withInput();
    }

}
