<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblAAMProductsCategory extends Model
{
    protected $table='tblAAMProductsCategory';
    protected $primaryKey='ProductsCategoryId';
    protected $fillable = ['CategoryName', 'Description', 'IsActive'];

    public function products() {
        return $this->hasMany('App\TblAAMProducts', 'ProductsCategoryId', 'ProductsCategoryId');
    }
}
