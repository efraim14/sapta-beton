<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::group(['prefix' => LaravelLocalization::setLocale()], function()
{

	Route::get('/', 'Index@home');

	Route::get('/home', 'Index@index')->name('home');

	Route::get('/gallery', 'Gallery@index')->name('gallery');

	Route::get('/products', 'Products@index')->name('products');

	Route::get('/career', 'Career@index')->name('career');

	Route::resource('index', 'Index');

	Route::resource('career', 'Career',['except' => ['index']]);

});

Auth::routes();

//ADMIN
Route::group(['prefix' => 'administrator'], function()
{
	Route::get('/', 'Admin@index')->name('admin');

	Route::get('/logout', 'Admin@logout')->name('logout');

	Route::get('/translations', 'Admin@translations')->name('translations');

	Route::get('/forms', 'Admin@forms')->name('forms');

	Route::get('/getForms', array('uses' => 'Admin@getForms'))->name('getForms');

	Route::resource('/clients', 'AdminClients',['except' => ['show']]);
	Route::get('/getServices', array('uses' => 'AdminClients@getServices'))->name('getServices');

	Route::resource('/gallery', 'AdminGallery',['except' => ['show']]);
	Route::get('/getGallery', array('uses' => 'AdminGallery@getGallery'))->name('getGallery');

	Route::resource('/gallery/category', 'AdminGalleryCategory',['except' => ['show', 'create', 'store']]);
	Route::get('/getGalleryCategory', array('uses' => 'AdminGalleryCategory@getGalleryCategory'))->name('getGalleryCategory');
});